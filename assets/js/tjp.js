function global()
	{
        
        if ($(window).width() <= 1024){
            $('.logo-jakartapost').appendTo('.showin-mobile-version');
            $('.action-user').appendTo('.showin-mobile-version');
            $('.tjp-related').appendTo('.main-single');
            $('.jobs').tjpslide({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                directionNav:true,
                itemMargin: 20,
                minItems: 2,
                maxItems: 2,
            });
        }
    $('.tjp-header .tjp-menu > li').hover(function(){
        $(this).find('.current').parent('li').toggleClass('hover');
    })


	}
function search_login_toggle()
    {


       var boxSearch = false;
       var boxLogin = false;
       var userLogged = false;
        
        $( ".btn-login-account" ).click(function(e) {
          e.preventDefault(); 
          $( ".action-user .login, .box-login" ).css("display","none");
          $( ".box-logged-in" ).css("display","block");
        }); 

        $( "li.logout a" ).click(function(e) {
          e.preventDefault(); 
          $( ".action-user .login" ).css("display","block");
          $( ".box-logged-in, .user-toggle" ).css("display","none");
        }); 

        $( ".box-search .search" ).click(function(e) {
           e.preventDefault();   
            if (boxSearch) {
                $( ".action-search" ).slideUp("600");
                boxSearch = false;
            }
            else {
                if(boxLogin || userLogged){
                    $( ".box-login, .user-toggle" ).slideUp("600",function(){
                        boxLogin = false;
                        userLogged = false;
                        boxSearch = true;
                        $( ".action-search" ).slideDown("600");
                    });
                } else {
                    boxSearch = true;
                    $( ".action-search" ).slideDown("600");
                }
            }
        }); 

        $( ".box-login" ).css("display","none");
        $( ".action-user .login" ).click(function(e){ 
            e.preventDefault();     
            if (boxLogin) {                
                $( ".box-login" ).slideUp("600");               
                boxLogin = false;
            }
            else {
                if(boxSearch){
                    $( ".action-search" ).slideUp("600",function(){
                        boxSearch = false;
                        boxLogin = true;
                        $( ".box-login" ).slideDown("600");
                    });
                } else {
                    boxLogin = true;
                    $( ".box-login" ).slideDown("600");
                }                            
            }
        }); 

        $( ".user-toggle, .box-logged-in" ).css("display","none");
        $( ".box-logged-in" ).click(function(e){ 
            e.preventDefault();     
            if (userLogged) {                
                $( ".user-toggle" ).slideUp("600");               
                userLogged = false;
            }
            else {
                if(boxSearch){
                    $( ".action-search" ).slideUp("600",function(){
                        boxSearch = false;
                        userLogged = true;
                        $( ".user-toggle" ).slideDown("600");
                    });
                } else {
                    userLogged = true;
                    $( ".user-toggle" ).slideDown("600");
                }                            
            }
        }); 




    }
function tab()
	{
		$('.tjp-tab li:first-child a').addClass('tjp-tab-current');
        $('.tjp-tab li a').on('click', function(e){
            e.preventDefault();

            if($(this).hasClass('tjp-tab-current')) {
            // do nothing because the link is already open
            } else {
            var oldcontent = $('.tjp-tab a.tjp-tab-current').attr('href');
            var newcontent = $(this).attr('href');

            $(oldcontent).fadeOut('fast', function(){
            $(newcontent).fadeIn().removeClass('hides');
            $(oldcontent).addClass('hides');
            });
            $('.tjp-tab a').removeClass('tjp-tab-current');
            $(this).addClass('tjp-tab-current');
            }
        });

        $('.tjp-tab-booking li:first-child a').addClass('tjp-tab-current');
        $('.tjp-tab-booking li a').on('click', function(e){
            e.preventDefault();

            if($(this).hasClass('tjp-tab-current')) {
            // do nothing because the link is already open
            } else {
            var oldcontent = $('.tjp-tab-booking a.tjp-tab-current').attr('href');
            var newcontent = $(this).attr('href');

            $(oldcontent).fadeOut('fast', function(){
            $(newcontent).fadeIn().removeClass('hides');
            $(oldcontent).addClass('hides');
            });
            $('.tjp-tab-booking a').removeClass('tjp-tab-current');
            $(this).addClass('tjp-tab-current');
            }
        });

        $('.tjp-tab-content .tjp-detail-tab').hide();
        $('.tjp-tab-content .tjp-detail-tab:first-child').show();
	}
function slide_gallery()
	{
		$(".tjp-slide-gallery").thumbnailScroller({ 
            scrollerType:"hoverAccelerate", 
            scrollerOrientation:"horizontal", 
            scrollSpeed:1, 
            scrollEasing:"easeOutCirc", 
            scrollEasingAmount:600, 
            acceleration:1, 
            scrollSpeed:800, 
            noScrollCenterSpace:10, 
            autoScrolling:0, 
            autoScrollingSpeed:200, 
            autoScrollingEasing:"easeInOutQuad", 
            autoScrollingDelay:500 
        });
	}
function slider_top()
	{
		$('.tjpslide').tjpslide({
            animation: "fade",
            directionNav: false,
            controlNav: true,
            start: function(slider){
              $('body').removeClass('loading');
            }
        });
	}
function job_portal()
	{
		$('.jobs').tjpslide({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            directionNav:true,
            itemWidth: 190,
            itemMargin: 19,
            minItems: 2,
            maxItems: 4,
        });
	}
function most_load()
    {
        $(".scroll").mCustomScrollbar();
    }
function discus_tab()
	{
		$('.tjp-discus-tab li:first-child a').addClass('tjp-tab-current');
        $('.tjp-discus-tab li a').on('click', function(e){
            e.preventDefault();

            if($(this).hasClass('tjp-tab-current')) {
            // do nothing because the link is already open
            } else {
            var oldcontent = $('.tjp-discus-tab a.tjp-tab-current').attr('href');
            var newcontent = $(this).attr('href');

            $(oldcontent).fadeOut('fast', function(){
            $(newcontent).fadeIn().removeClass('hides');
            $(oldcontent).addClass('hides');
            });
            $('.tjp-discus-tab a').removeClass('tjp-tab-current');
            $(this).addClass('tjp-tab-current');
            }
        });
        $('.tjp-detail-discus .tjp-comment-tab').hide();
        $('.tjp-detail-discus .tjp-comment-tab:first-child').show();
	}
function menu_mobile()
    {
        $('body').append('<div class="tjp-mobile-menu-container"/>');
        $('.tjp-menu-mobile').find('ul').clone().addClass('tjp-menu-panel').appendTo('.tjp-mobile-menu-container').find('ul').remove();
        $('.tjp-toggle-menu').on('click touch', function () { $('html').toggleClass('tjp-menu-active')});
        $('.tjp-btn-enter').on('click touch', function () {
            $('.tjp-menu-panel').removeClass('tjp-active-ul');
            $('.tjp-menu-panel li').removeClass('tjp-active-li');
            var itemPath = $(this).parent().attr('class').replace("li", "ul");
            var itemParent = $(this).parent().attr('class').replace("li", "ul").split('-');
            var spliced = itemParent.splice(-1, 1);
            var itemParent = itemParent.join("-");
            $('.tjp-menu-panel').removeClass('tjp-active-leave tjp-parent-leave tjp-active-enter tjp-parent-enter');
            $('.tjp-menu-panel.' + itemParent).addClass('tjp-parent-enter');
            $('.tjp-menu-panel.' + itemPath).addClass('tjp-active-enter');
        });
        $('.tjp-leave-bar').on('click touch', function () {
            var itemParent = $(this).parent().attr('class').replace("li", "ul").split('-');
            var splicedParent = itemParent.splice(-1, 1);
            var splicedParent = itemParent.splice(-1, 1);
            var itemParent = itemParent.join("-");
            var itemPath = $(this).parent().attr('class').replace("li", "ul").split('-');
            var spliced = itemPath.splice(-1, 1);
            var itemPath = itemPath.join("-");
            $('.tjp-menu-panel').removeClass('tjp-active-leave tjp-parent-leave tjp-active-enter tjp-parent-enter');
            $('.tjp-menu-panel.' + itemParent).addClass('tjp-parent-leave');
            $('.tjp-menu-panel.' + itemPath).addClass('tjp-active-leave'); 
        });
    }
function paging()
    {
        $(".navigation-page").jPages({
            containerID: "tjp-control-paging",
            perPage      : 2,
            first       : false,
            previous    : "previous",
            next        : "next",
            last        : false
        });
    }
function paging_index()
    {
        $(".navigation-page").jPages({
            containerID: "tjp-control-paging",
            perPage      : 15,
            first       : 'first',
            previous    : false,
            next        : false,
            last        : 'last'
        });
    }
function detail_slide()
    {
        $('#slide-detail').tjpslide({
            animation: "fade",
            controlNav: false,
            animationLoop: false,
            directionNav: false,
            slideshow: false,
            sync: "#thumb-detail-slide"
        });
        $('#thumb-detail-slide').tjpslide({
            animation: "slide",
            controlNav: false,
            itemWidth: 90,
            itemMargin:0, 
            maxItems: 9,
            minItems:9,
            // direction: "vertical",
            asNavFor: '#slide-detail',
        });
    }
function detail_video()
    {
        $('#slide-detail').tjpslide({
            animation: "fade",
            controlNav: false,
            animationLoop: false,
            directionNav: false,
            slideshow: false,
            sync: "#thumb-detail-slide"
        });
        $('#thumb-detail-slide').tjpslide({
            animation: "slide",
            controlNav: false,
            itemWidth: 90,
            itemMargin:0, 
            maxItems: 3,
            minItems:3,
            direction: "vertical",
            asNavFor: '#slide-detail',
        });
        $('#thumb-detail-slide .slides').width('100%');
        var targetHeight = jQuery('.slide-video').css('height');
        targetHeight = parseInt(targetHeight, 10);
        jQuery('.slide-video').attr('style', 'height:' + targetHeight + 'px' + ' !important;' + '        overflow: hidden; position: relative');   
        jQuery('.rotating-leader img').attr('style', 'height: ' + targetHeight / 4 + 'px' + ' !important');
    }
function paging_travel()
    {
        $(".navigation-page").jPages({
            containerID: "tjp-control-paging",
            perPage      : 4,
            first       : 'first',
            previous    : false,
            next        : false,
            last        : 'last'
        });
    }
function sea()
    {
        $('.tjp-flag .ic-all').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-all').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-brunei').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-brunei').show();
            $('.flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-myanmar').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-myanmar').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-cambodja').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-cambodja').show();
            $('.flag-latest .ic-myanmar, .flag-latest .ic-brunei, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-timor').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-timor').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-laos').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-laos').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-malaysia').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-malaysia').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-filipina').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-filipina').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-singapura, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-singapura').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-singapura').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-thailand, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-thailand').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-thailand').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-vietnam').hide();
        });
        $('.tjp-flag .ic-vietnam').click(function(e){
            e.preventDefault();
            $('.flag-latest .ic-vietnam').show();
            $('.flag-latest .ic-brunei, .flag-latest .ic-myanmar, .flag-latest .ic-cambodja, .flag-latest .ic-timor, .flag-latest .ic-laos, .flag-latest .ic-malaysia, .flag-latest .ic-filipina, .flag-latest .ic-singapura, .flag-latest .ic-thailand').hide();
        });

    }
function slidergallery() {
            
    var jssor_1_options = {
      $AutoPlay: true,
      $SlideWidth: 600,
      $Cols: 2,
      $Align: 100,
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$
      }
    };
    
    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
    
    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizes
    function ScaleSlider() {
        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        if (refSize) {
            refSize = Math.min(refSize, 1067);
            jssor_1_slider.$ScaleWidth(refSize);
        }
        else {
            window.setTimeout(ScaleSlider, 100);
        }
    }
    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);
    $Jssor$.$AddEvent(window, "resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    //responsive code end
}

function save_img_preview(position,image_64){
    var url = "upload_preview.php";
    var options = {
        url:url,
        type:"post",
        dataType: 'json',
        data: {
            base64data : image_64,
            position:position
        },
        success:function(data){
            console.log(data);
        },
        beforeSend:function(){
            
        }
    }
    $.ajax(options);

} 

jQuery.fn.extend({
  fmDrop: function() {
    return this.each(function() {
        var _this = this;
        var _position = this.getAttribute('data-position');
        this.addEventListener("dragenter", function(e){
            e.stopPropagation();
            e.preventDefault();
        }, false);
        this.addEventListener("dragover", function(e){
            e.stopPropagation();
            e.preventDefault();
        }, false);
        this.addEventListener("drop", function(e){
            e.stopPropagation();
            e.preventDefault();
            var dt = e.dataTransfer;
            var images = dt.files;
            var image = images[0];
            var reader = new FileReader();
            reader.readAsDataURL(image);
            reader.addEventListener("loadend", function(){

                var imageElement = document.createElement("img");  

                imageElement.src = this.result;
                // save_img_preview(_position,this.result);
                imageElement.onload = function() {
                    console.log(this.width);
                    console.log(this.height);
                };
                _this.appendChild(imageElement);
            }, false);


        }, false);
    });
}
});

function delegate_banner(){
    $('.dropview').each(function(){
        if($(this).find('img').length > 0){
            var pos = $(this).attr('data-position');
            var src = $(this).find('img').attr('src');
            $('#iframe').find('img[data-target="'+pos+'"]').attr('src',src);
        }
    });
}

function close_popup(){
$(".close-popup").click(function(e){
    e.preventDefault();
    $('.ads-popup-preview').removeClass('ads-popup-visible');
});
}

function popup(){
        if($('.ads-popup-preview').hasClass('ads-popup-visible')){
            //alert("dsadsadasdsad");
            $('.ads-popup-preview').removeClass('ads-popup-visible');

        } else {
            $('.ads-popup-preview').addClass('ads-popup-visible');
            //$( ".ads-left" ).clone().appendTo( ".main-adver-lr.to-left" );
            //$( ".ads-right" ).clone().appendTo( ".main-adver-lr.to-right" );
        }

}

function clearDropView(){
    $('.dropview').each(function(){
        $(this).find('img').remove();
    });
}

function select_ads(){
    var arr_type        = ["","Web Banner","Sponsor"];
    var arr_res         = ["","Desktop","Tablet","Mobile"];
    var arr_channel     = ["","Homepage","Single","Channel"];

    var target_type     = ["","webbanner","sponsor"];
    var target_res      = ["","desktop","tablet","mobile"];
    var target_channel_webbanner    = ["","homepage","singlepage","channelpage"];
    var target_channel_sponsor      = ["","advertorial","inforial","videoads"];

    var type    = $("input#ads-type").val();
    var res     = $("input#ads-res").val();
    var channel = $("input#ads-channel").val();
    var avail   = $("input#ads-avail").val();
    var concat  = type+res+channel;


    $('.pagetype-wrap').removeClass('active');
    $('#pagetype-wrap-'+target_type[type]).addClass('active');
    
    $('#pagetype-wrap-'+target_type[type]+' .resolution-wrap').removeClass('active');
    var id_res = 'resolution-'+target_type[type]+'-'+target_res[res];
    $('#pagetype-wrap-'+target_type[type]+' #'+id_res).addClass('active');
    $('.'+target_type[type]+'-pagetype').removeClass('active');
    if(type == 1){
        var channel_target = target_channel_webbanner[channel];
    } else {
        var channel_target = target_channel_sponsor[channel];
    }
    //$('.webbanner-pagetype').removeClass('active');
    $('#'+target_type[type]+'-'+channel_target).addClass('active');

    $('#'+id_res).find('.webbanner-pagetype').removeClass('active');
    $('#'+id_res).find('#'+target_type[type]+'-'+channel_target).addClass('active');

    // if(type == 1){
    //     if(channel == 2){
    //         $('.webbanner-pagetype').removeClass('active');
    //         $('#webbanner-singlepage').addClass('active');
    //     } else if(channel == 3){
    //         $('.webbanner-pagetype').removeClass('active');
    //         $('#webbanner-channelpage').addClass('active');
    //     } else {
    //         $('.webbanner-pagetype').removeClass('active');
    //         $('#webbanner-homepage').addClass('active');
    //     }
    //     $('.pagetype-wrap').removeClass('active');
    //     $('#pagetype-wrap-webbanner').addClass('active');
    // } else {
    //     if(channel == 2){
    //         $('.sponsor-pagetype').removeClass('active');
    //         $('#sponsor-inforial').addClass('active');
    //     } else if(channel == 3){
    //         $('.sponsor-pagetype').removeClass('active');
    //         $('#sponsor-videoads').addClass('active');
    //     } else {
    //         $('.sponsor-pagetype').removeClass('active');
    //         $('#sponsor-advertorial').addClass('active');
    //     }
    //     $('.pagetype-wrap').removeClass('active');
    //     $('#pagetype-wrap-sponsor').addClass('active');
    // }
    

    // if(concat == "111"){
    //     $('.tjp-tab-content .tjp-detail-tab').hide();
    //     $('.tjp-tab-content #news').show();
    //     //alert("dasdasdasdsad");
    // } else {
    //     var type_str    = arr_type[type];
    //     var res_str     = arr_res[res];
    //     var channel_str = arr_channel[channel];
    //     var text_content = type_str+" - "+res_str+" - "+channel_str;
    //     $('#no-design-caption').html(text_content);
    //     $('.tjp-tab-content .tjp-detail-tab').hide();
    //     $('.tjp-tab-content #sea').show();
    // }
    clearDropView();
    console.log(concat);
}

$(document).ready(function(){

    //$('.nav-tabs').tab();
    $('#webbanner-sp-tabs a').on('click', function() {
        $(this).tab('show');
    });
    $('.dropview').fmDrop();


       $(".btn-popup-ads").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        $('#iframe').load(href+' .tjp-page',function(){
            delegate_banner();
            popup();
            search_login_toggle();
            close_popup();
            $('#iframe').css('display','');
        })
    });


    
       $("ul.tab-sidebar li a").click(function(e){
            e.preventDefault();
            var same_rel = $(this).attr("id"); 
            $('ul.tab-sidebar li a.active').removeClass('active');
            $(this).addClass('active');
            $('.tab-content .detail-tab.active').removeClass('active');
            $('.tab-content div[id="'+same_rel+'"]').addClass('active');

            var val = $(this).attr('data-value');
            $("input#ads-type").val(val);
            select_ads();
        
       });


       $(".webbanner-pagetype ul.nav-tabs li").click(function(e){
            e.preventDefault();
            var same_rel = $(this).children('a').attr("rel"); 
            //$('.webbanner-pagetype ul.nav-tabs li.active').removeClass('active');
            //$(this).addClass('active');
            $('.webbanner-pagetype .tab-content .tab-pane.active').removeClass('active');
            $('.webbanner-pagetype .tab-content .tab-pane[rel="'+same_rel+'"]').addClass('active');

       });

       $("ul.tab-inner.one li a").click(function(e){
            e.preventDefault();
            $('ul.tab-inner.one li a.active').removeClass('active');
            $(this).addClass('active');  

            var val = $(this).attr('data-value');
            $("input#ads-res").val(val);
            select_ads();      
       });
       
       $("ul.tab-inner.two li a").click(function(e){
            e.preventDefault();
            $('ul.tab-inner.two li a.active').removeClass('active');
            $(this).addClass('active');   

            var val = $(this).attr('data-value');
            $("input#ads-res").val(val);
            select_ads();        
       });

       $("ul.list-group li").click(function(e){
            e.preventDefault();
            $('ul.list-group li.active').removeClass('active');
            $(this).addClass('active');   

            var val = $(this).attr('data-value');
            $("input#ads-channel").val(val);
            select_ads();        
       });

       $("ul.tab-right li a").click(function(e){
            e.preventDefault();
            $('ul.tab-right li a.active').removeClass('active');
            $(this).addClass('active');   
            var val = $(this).attr('data-value');
            $("input#ads-avail").val(val);
            select_ads();       
       });
});