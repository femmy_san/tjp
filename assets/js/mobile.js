/*!
*   tjp jquery mobile menu
*   v 1.1
*   Copyright (c) 2015 Tomasz Kalinowski
*   http://mobile-menu.tjp.pl
*   GitHub: https://github.com/ma-5/tjp-mobile-menu
*/
function tjpmobileMenu(atributes) {
    if(atributes.swipe == true) {
        $('html').addClass('tjp-menu-swipe');
        $('body').wrapInner('<div class="tjp-page" />');
        $('body').append('<div class="tjp-mobile-menu-container"><div class="tjp-close-bar"></div></div>');
        $('.tjp-menu-header .tjp-toggle-menu').clone().appendTo('.tjp-close-bar');
        $('.tjp-menu-heading').clone().appendTo('.tjp-close-bar');
    } else {
        $('body').append('<div class="tjp-mobile-menu-container"/>');
    }
    $('.tjp-menu-mobile').find('ul').clone().addClass('tjp-menu-panel').appendTo('.tjp-mobile-menu-container').find('ul').remove();
    $('.tjp-toggle-menu').on('click touch', function () { $('html').toggleClass('tjp-menu-active')});
    $('.tjp-btn-enter').on('click touch', function () {
        $('.tjp-menu-panel').removeClass('tjp-active-ul');
        $('.tjp-menu-panel li').removeClass('tjp-active-li');
        var itemPath = $(this).parent().attr('class').replace("li", "ul");
        var itemParent = $(this).parent().attr('class').replace("li", "ul").split('-');
        var spliced = itemParent.splice(-1, 1);
        var itemParent = itemParent.join("-");
        $('.tjp-menu-panel').removeClass('tjp-active-leave tjp-parent-leave tjp-active-enter tjp-parent-enter');
        $('.tjp-menu-panel.' + itemParent).addClass('tjp-parent-enter');
        $('.tjp-menu-panel.' + itemPath).addClass('tjp-active-enter tjp-active-ul');
    });
    $('.tjp-leave-bar').on('click touch', function () {
        $('.tjp-menu-panel').removeClass('tjp-active-ul');
        var itemParent = $(this).parent().attr('class').replace("li", "ul").split('-');
        var splicedParent = itemParent.splice(-1, 1);
        var splicedParent = itemParent.splice(-1, 1);
        var itemParent = itemParent.join("-");
        var itemPath = $(this).parent().attr('class').replace("li", "ul").split('-');
        var spliced = itemPath.splice(-1, 1);
        var itemPath = itemPath.join("-");
        $('.tjp-menu-panel').removeClass('tjp-active-leave tjp-parent-leave tjp-active-enter tjp-parent-enter');
        $('.tjp-menu-panel.' + itemParent).addClass('tjp-parent-leave tjp-active-ul');
        $('.tjp-menu-panel.' + itemPath).addClass('tjp-active-leave');
    });
}
