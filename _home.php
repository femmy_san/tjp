<!DOCTYPE html>
<html lang="en">
<?php include('include/meta.php');?>
    <body class="homepage">
        
        <div class="container no-padding tjp-page">
            <div class="col-xs-12 no-padding wrapper">

                <?php include('include/header.php');?>
                
                <div class="main-adver-lr to-left no-padding">
                    <img data-target="left" src="assets/img/banner-lr.jpg">
                </div>
                
                
                <?php include('content/home.php');?>

                <?php include('include/scroller.php');?>

                <?php include('include/travel.php');?>
 

 
                <div class="main-adver-lr to-right no-padding">
                   <!-- <img data-target="left" src="assets/img/banner-lr.jpg"> -->
                </div>

             <?php include('include/footer.php');?>
                
            </div> <!-- .wrapper -->
        </div> <!-- .tjp-page -->
        
        <?php include('include/bottom.php');?>

    </body>
</html>