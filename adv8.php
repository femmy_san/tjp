<!DOCTYPE html>
<html lang="en">
<?php include('include/meta.php');?>
    <body class="add-with-us-3">
        
        <div class="container">
            <div class="row" style="margin-top:50px;">
                <div class="col-sm-5">
                    <form>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" id="field-title" name="title" class="form-control" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <input id="field-image" type="file">
                            <p class="help-block">Example block-level help text here.</p>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea name="content" id="field-content" rows="10" class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="col-sm-offset-1 col-sm-5" id="area-preview">
                    <h3>Title Advertise Here</h3>
                    <img id="preview-image" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150">
                    <p style="margin-top:25px;">Content Here...</p>
                </div>
            </div>
            
        </div>


        <?php include('include/bottom.php');?>
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
        <script type="text/javascript">
            function nl2br (str, is_xhtml) {   
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
            }

            document.getElementById('field-image').onchange = function (evt) {
                var tgt = evt.target || window.event.srcElement,
                    files = tgt.files;

                // FileReader support
                if (FileReader && files && files.length) {
                    var fr = new FileReader();
                    fr.onload = function () {
                        document.getElementById("preview-image").src = fr.result;
                    }
                    fr.readAsDataURL(files[0]);
                }

                // Not supported
                else {
                    // fallback -- perhaps submit the input to an iframe and temporarily store
                    // them on the server until the user's session ends.
                }
            }

            $(document).ready(function(){
                $( "#field-title" ).keyup(function() {
                  var val = $(this).val();
                  if(val==""){
                    val = "Title Advertise here";
                  }
                  $('#area-preview').find('h3').text(val);
                });

                // $('#field-image').change(function(){
                //     console.log("dasdsad");
                //     readURL(this);
                // });

                $( "#field-content" ).keyup(function() {
                    var val = $(this).val();
                    val = nl2br(val,false);
                    if(val==""){
                        val = "Content Advertise here";
                    }
                    $('#area-preview').find('p').html(val);
                });
            });
        </script>
    </body>
</html>