             <div class="col-xs-12 tjp-footer">
                    <div class="main-center">
                        <div class="col-md-9 col-xs-12 no-padding">
                            <div class="col-xs-12 no-padding">
                                <div class="col-md-4 col-xs-12 columns no-padding">
                                    <a href="#" class="pad-bil"><img src="assets/img/logo-footer.png" width="233" height="30" alt="Jak Post" /></a>
                                </div>
                                <div class="col-md-4 col-xs-12 link-footer">
                                    <a href="#">Contact Us</a>
                                    <a href="#">Media Kit</a>
                                    <a href="#">clads Index</a>
                                </div>
                                <div class="col-md-4 col-xs-12 copy-footer">
                                    <p>&copy; 2015 PT Niskala Media Tenggara</p>
                                </div>
                                <div class="col-xs-12 partner-footer">
                                    <h2 class="title-partner col-md-4 no-padding">
                                        Our partner
                                    </h2>
                                    <ul class="large-block-grid-4 col-md-8">
                                        <li>
                                            <a href="#"><img src="assets/img/logo-partner-1.jpg" width="37" height="33" alt="Jakarta Post"></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="assets/img/logo-partner-2.jpg" width="77" height="33" alt="Jakarta Post"></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="assets/img/logo-partner-3.jpg" width="37" height="33" alt="Jakarta Post"></a>
                                        </li>
                                        <li>
                                            <a href="#"><img src="assets/img/logo-partner-2.jpg" width="77" height="33" alt="Jakarta Post"></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 no-padding">
                                    <div class="col-md-7 col-xs-12 tjp-sosmed">
                                        <ul class="large-block-grid-3">
                                            <li class="news-footer">
                                                <span>News</span>
                                                <ul class="icon-soscmed">
                                                    <li>
                                                        <a href="#" class="fa fa-fw facebook">&#xf09a;</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="fa fa-fw linked">&#xf0e1;</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="fa fa-fw twitter">&#xf099;</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="fa fa-fw google">&#xf0d5;</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="travel-footer">
                                                <span>TRAVEL</span>
                                                <ul class="icon-soscmed">
                                                    <li>
                                                        <a href="#" class="fa fa-fw instagram">&#xf16d;</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="fa fa-fw pint">&#xf231;</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="jobs-footer">
                                                <span>Jobs</span>
                                                <ul class="icon-soscmed">
                                                    <li>
                                                        <a href="#" class="fa fa-fw linked">&#xf0e1;</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="fa fa-fw twitter">&#xf099;</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 col-xs-12 suscribe">
                                        <form>
                                            <input type="text" placeholder="your email for more news" />
                                            <input type="submit" value="SUBSCRIBE">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>