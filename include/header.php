                      <div class="on-ie-underversion9">
                        <div class="col-xs-6 article-ie">
                            <img src="" alt="Jakarta Post" />
                            <p>Please Update your browser</p>
                            <p>
                              Your browser is out of date, and may not be compatible with our website. A list of the most popular web browsers can be found below.<br />
                              Just click on the icons to get to the download page.</p>
                              <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">
                                <img src="assets/img/browser_msie.gif" alt="Jakarta Post" />
                            </a>
                        </div><!-- End article-ie -->
                    </div><!-- End on-ie-underversion9 -->




                    <div class="main-center">
                        <div class="col-xs-12 no-padding showin-mobile-version">
                            <div class="col-xs-2 btn-toggle current">
                                <a class="btn btn-primary tjp-toggle-menu"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 menu-active-breadcrumbs news-current title-page-on-mobile">

                        </div>
                        <div class="banner-top-head col-xs-12 no-padding">
                            <a href="#">
                                <img data-target="top" src="assets/img/banner-header-top.jpg" alt="Jakarta Post">
                            </a>    
                        </div>
                        
                        <div class="jak-pos-contact col-xs-12 no-padding">
                            <div class="col-xs-9 no-padding">
                                <span>Subscription Services: Phone: +62 21 7030 0990</span>
                            </div>
                            <div class="action-user col-md-3 col-xs-3 no-padding">
                                
                                <div class="search-login">
                                 <div class="box-search">
                                    <a href="#" class="fa fa-search search"></a>
                                 </div>
                                 <div class="login-box">
                                    <a href="#login" class="fa fa-user login"></a>
                                 </div>
                                </div>

                                <div class="box-logged-in">
                                 <a href="#" class="logged-in">
                                     <div class="user-avatar"><img src="assets/img/icon/user-avatar.png"></div>
                                     <div class="user-greeting">
                                        <p>Hello,<span>Username</span></p>
                                     </div>
                                 </a>
                                </div>
                            </div>
                        </div>
                        <div class="action-search-wrapper">
                         <div class="action-search col-xs-12 no-padding">
                            <form>
                                <input type="text" placeholder="Search.." />
                                <input type="submit" value="" />
                            </form>
                         </div>
                        </div>
                        <div class="tjp-popup ">
                            <div id="login" class="box-login signup">
                                <form action="#">
                                    <div class="form-group">
                                       <input type="text" id="email-top" placeholder="Email/Username" />
                                   </div>
                                   <div class="form-group">
                                       <input type="password" id="password-top" placeholder="Password" />
                                   </div>
                                   <a href="#" class="forgot-form">Forgot password ?</a>

                                   <button type="submit" class="btn btn-red btn-login-account">login</button>

                                   <span>Don’t have account? <a href="#">Register Here</a></span>


                               </form>
                               <div class="social-login">
                                <h3>Or Login with your social account</h3>
                                <a class="btn btn-fb" href="#" role="button"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                                <a class="btn btn-g-plus" href="#" role="button"><i class="fa fa-google-plus"></i><span>Google+</span></a>
                                <a class="btn btn-linkedln" href="#" role="button"><i class="fa fa-linkedin"></i><span>Linkedln</span></a>

                                
                            </div>
                         </div>
                        </div>
                        <div class="user-toggle-wrapper">
                          <div class="user-toggle text-center"> 
                            <a href="#" class="image-box">
                              <img src="assets/img/icon/user-ava.png" alt="">                          
                            </a>
                            <h2>Username</h2>
                            <a class="btn btn-default btn-article" href="#" role="button">NEW ARTICLE</a>
                            <ul class="list-inline">
                              <li class="profile"><a href="#">My Profile</a></li>
                              <li class="logout"><a href="#">Logout</a></li>
                            </ul>
                          </div> 
                        </div>
                        <div class="logo-jakartapost col-md-12 col-xs-7 no-padding">
                            <a href="index.html">
                                <img src="assets/img/logo.jpg" alt="Jakarta Post">
                            </a>
                        </div>
                        <div class="col-xs-12 tjp-header no-padding tjp-menu-mobile">
                            <ul class="block-grid-7 tjp-menu tjp-ul">
                                <li class="tjp-li-1 tjp-dhide"><a href="#">Home</a></li>
                                <li class="tjp-li-2 tjp-dhide"><a href="intergrated-single-page.html">NEWS PULSE</a></li>
                                <li class="tjp-li-3 tjp-dhide"><a href="#">MULTIMEDIA</a></li>
                                <li class="tjp-li-4 tjp-dhide"><a href="#">MOST VIEWED</a></li>
                                <li class="tjp-li-5 tjp-dhide"><a href="#">MOST COMMENTED</a></li>
                                <li class="tjp-li-6">
                                    <a href="#" class="tjp-has-submenu current">news</a>
                                    <span class="tjp-btn-enter"></span>
                                    <ul class="tjp-ul-6">
                                        <li class="tjp-li-6-1">
                                            <div class="tjp-leave-bar"><span class="tjp-btn-leave"></span>news</div>
                                            <a href="#">POLITICS</a>
                                        </li>
                                        <li class="tjp-li-6-2">
                                            <a href="#">BUSINESS</a>
                                        </li>
                                        <li class="tjp-li-6-3">
                                            <a href="#">GLOBAL</a>
                                        </li>
                                        <li class="tjp-li-6-4">
                                            <a href="#">NATIONAL</a>
                                        </li>
                                        <li class="tjp-li-6-5">
                                            <a href="#">CITY</a>
                                        </li>
                                        <li class="tjp-li-6-6">
                                            <a href="#">LONGFORM</a>
                                        </li>
                                        <li class="tjp-li-6-7">
                                            <a href="#">MULTIMEDIA</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="tjp-li-7"><a href="#">SOUTH EAST ASIA</a></li>
                                <li class="tjp-li-8"><a href="#">COMMUNITY</a></li>
                                <li class="tjp-li-9">
                                    <a href="#" class="tjp-has-submenu">ACADEMIA</a>
                                    <span class="tjp-btn-enter"></span>
                                    <ul class="tjp-ul-9">
                                        <li class="tjp-li-9-1">
                                            <div class="tjp-leave-bar">
                                                <span class="tjp-btn-leave">
                                                    <span class="glyphicon glyphicon-menu-left"></span>
                                                </span>
                                                ACADEMIA
                                            </div>
                                            <a href="#">article</a>
                                        </li>
                                        <li class="tjp-li-9-2"><a href="#" class="">working paper</a></li>
                                        <li class="tjp-li-9-3"><a href="#">event</a></li>
                                        <li class="tjp-li-9-4"><a href="#">video</a></li>
                                    </ul>
                                </li>
                                <li class="tjp-li-10"><a href="#">LIFE</a></li>
                                <li class="tjp-li-11">
                                    <a href="#" class="tjp-has-submenu">TRAVEL</a>
                                    <span class="tjp-btn-enter"></span>
                                    <ul class="tjp-ul-11 current-page">
                                        <li class="tjp-li-11-1">
                                            <div class="tjp-leave-bar"><span class="tjp-btn-leave"></span>TRAVEL</div>
                                            <a href="#">INSPIRATION</a>
                                        </li>
                                        <li class="tjp-li-11-2"><a href="#">BOOKING DIRECTORY</a></li>
                                        <li class="tjp-li-11-3"><a href="#">VIDEO</a></li>
                                        <li class="tjp-li-11-4"><a href="#">PHOTO</a></li>
                                    </ul>
                                </li>
                                <li class="tjp-li-12"><a href="#">JOBS</a></li>
                            </ul>
                        </div>
                       

                    </div>

                    <!-- <div class="col-xs-12 banner-show">
                        <a href="#">
                            <img src="assets/img/advrt-menu.jpg" alt="Jakarta Post">
                        </a>
                    </div> -->