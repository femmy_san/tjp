        <!--<script src="assets/js/jquery.min.js"></script>-->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery-ui.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/mobile.js"></script>
        
        <script src="assets/js/tjp.js"></script>
        <script src="assets/js/slider.js"></script>
        <script src="assets/js/scroll.js"></script>
        <script src="assets/js/scroll-gallery.js"></script>
        <script src="assets/js/paging.js"></script>
        <script src="assets/js/jquery.touchSwipe.min.js"></script>

        <script type="text/javascript">
            $(window).load(function(){
                most_load();
                job_portal();
                search_login_toggle();
                popup();
                close_popup();
            });
            $(document).ready(function(){
                global();
                tab();
                slide_gallery();
                paging_index();
                tjpmobileMenu({
                    swipe: true
                });
                search_login_toggle();
                popup();
                close_popup();
            });
        </script>
        <script type="text/javascript">
            var availableTags = [
                "Jakarta (CGK)",
                "Yogyakarta (JOG)",
                "Bandung (BDG)",
                "Surabaya (SBY)",
            ];
            $( "#dari" ).autocomplete({
                source: availableTags,
                open: function() { $('.ui-menu').width(280) }  
            });
            $( "#tujuan" ).autocomplete({
                source: availableTags,
                open: function() { $('.ui-menu').width(280) }  
            });
            $( "#datepicker" ).datepicker({
                inline: true
            });
        </script>