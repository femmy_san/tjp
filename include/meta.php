<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Homepage</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/css/font-awesome.css" rel="stylesheet" >
    <link href="assets/css/slider.css" rel="stylesheet">
    <link href="assets/css/build/tjp.css" rel="stylesheet">
    <link href="assets/css/scroll.css" rel="stylesheet">
    <link href="assets/css/jquery-ui.min.css" rel="stylesheet">
    <link href="assets/css/scroll-gallery.css" rel="stylesheet">
    <link href="assets/css/paging.css" rel="stylesheet">
    <script type="text/javascript" src="assets/js/custom.modernizr.js"></script>

    <!---  Femmy Extend CSS-->
    <style type="text/css">
        .pagetype-wrap {
            display:none;
        }
        .pagetype-wrap.active {
            display:block;
        }
        
        .resolution-wrap {
            display:none;
        }
        .resolution-wrap.active {
            display:block;
        }

        .webbanner-pagetype {
            display:none;
        }

        .webbanner-pagetype.active {
            display:block;
        }

        .sponsor-pagetype {
            display:none;
        }

        .sponsor-pagetype.active {
            display:block;
        }
    </style>

    <!---  Femmy Extend CSS-->
</head>