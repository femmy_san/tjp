<!DOCTYPE html>
<html lang="en">
<?php include('include/meta.php');?>
    <body class="add-with-us-3">
        
        <div class="container no-padding tjp-page">
            <div class="col-xs-12 no-padding wrapper">

                <?php include('include/header.php');?>
                
                <?php include('include/breadcrumbs/breadcrumbs_ads.php');?>
                
                
                <?php include('content/addwithusnew3.php');?>


                <?php include('include/footer.php');?>
                
            </div> <!-- .wrapper -->
        </div> <!-- .tjp-page -->
        
        <div class="ads-popup-preview">
            <div class="ads-popup-preview-inner">
                <a href="#" class="close-popup">x</a>
                <div id="iframe" style="overflow:hidden;display:none;height:100%;width:100%">
                    <!-- <iframe src="http://localhost/tjp/_home.php" frameborder="0" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%"></iframe> -->
                </div>
            </div>

        </div>
        <?php include('include/bottom.php');?>



    </body>
</html>