<?php $cp_contents = array('news','sea','community');?>
<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach($cp_contents as $idx => $cp_content):?>
            <li role="presentation" class="<?php echo $idx==0?'active':'';?>"  data-toggle="tab">
                <a href="#tjp-singlepage-<?php echo $cp_content;?>" aria-controls="<?php echo $cp_content;?>" role="tab" data-toggle="tab" rel="<?php echo $cp_content;?>">
                    <?php $title = $cp_content=='sea'?'South East Asia':$cp_content; ?>
                    <?php echo ucfirst($title);?>
                </a>
            </li>
        <?php endforeach;?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php foreach($cp_contents as $idx => $cp_content):?>
            <div role="tabpanel" class="tab-pane <?php echo $idx==0?'active':'';?>" id="#tjp-channelpage-<?php echo $cp_content;?>" rel="<?php echo $cp_content;?>">
                <?php $title = $cp_content=='sea'?'South East Asia':$cp_content; ?>
                <h4 class="caption-no-content"><?php echo ucfirst($title);?></h4>
                <?php $inc_path = $cp_content.'-tab.php';?>
                <?php include($inc_path);?>
            </div>
        <?php endforeach;?>
    </div>

</div>