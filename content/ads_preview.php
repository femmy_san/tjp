<div class="main-center content">
 <div class="row">
 
 	<div class="col-xs-12 col-sm-3 left">
 		<h2>Choose Channel :</h2>
 		<ul class="list-group">
 			<li class="list-group-item active"><a href="#">News</a></li>
 			<li class="list-group-item"><a href="#">Travel</a></li>
 			<li class="list-group-item"><a href="#">Community</a></li>
 			<li class="list-group-item"><a href="#">South East Asia</a></li>
 			<li class="list-group-item"><a href="#">Academia</a></li>
 			<li class="list-group-item"><a href="#">Youth</a></li>
 		  	<li class="list-group-item"><a href="#">Multimedia</a></li>
 		  	<li class="list-group-item"><a href="#">Life</a></li>
 		  	 <li class="list-group-item"><a href="#">Jobs</a></li>
 		  </ul>
 	     <h5> For further info please contact :</h5>

       <h6>Aries Saputra</h6>
       <p> (62) 21 5300-476/78</p>
       <a href="#">aries@thejakartapost.com</a>
     </div> <!-- .left -->


     <div class="col-xs-12 col-sm-9 right">
        <div class="simulate">
        		<div class="row">
        			<div class="col-xs-12 top">
        				<a href="#"> <img src="assets/img/logo.jpg"></a>
        			</div> <!-- .col-xs-12 -->
                </div> <!-- .row -->
                <div class="row">
        			<div class="col-xs-2 left ads-left">
        				<div class="dropview" data-position="left">
                            <?php if(file_exists('./preview/left.png')):?>
                                <img src="preview/left.png">
                            <?php endif;?>
                        </div>
        
        	        </div>   <!-- .left -->

        	        <div class="col-xs-8 center">
        	        	<div class="box-wrap">
        	        		<div class="box-ads-1 ads-center-1">
        	        			<div class="dropview" data-position="top">
                                    <?php if(file_exists('./preview/top.png')):?>
                                        <img src="preview/top.png">
                                    <?php endif;?>            
                                </div>
        	        	    </div>
        	        	    <div class="box-ads-2 ads-center-2">
        	        	    	<div class="dropview" data-position="center">
                                    <?php if(file_exists('./preview/center.png')):?>
                                        <img src="preview/center.png">
                                    <?php endif;?> 
                                 </div>
        	        	    </div>
        	        	</div>
        	        	
        	        </div>   <!-- .center -->
        	        <div class="col-xs-2 right ads-right">
        	        	<div class="dropview" data-position="right">
                            <?php if(file_exists('./preview/right.png')):?>
                                <img src="preview/right.png">
                            <?php endif;?>       
                        </div>
        	        </div>   <!-- .right -->
               </div> <!-- .row -->
        </div>   <!-- .simulate -->
        <!-- <a class="btn btn-default btn-red btn-popup-ads" href="#" role="button">Generate Preview</a> -->
        <a class="btn btn-default btn-red" href="_ads_preview.png" target="_blank" role="button">Generate Preview</a>
     </div> <!-- .right -->

<style type="text/css">
.dropview{width: 100%; height: 100%; text-align: center; background: #ccc;}
</style>


</div>
</div>
