<div id="resolution-webbanner-<?php echo $resol;?>" class="resolution-wrap <?php echo $idx==0?'active':'';?>">
    <p><?php echo $resol;?></p>
    <div id="webbanner-homepage" class="webbanner-pagetype active">
        <?php include('content/homepage/index.php');?>
    </div>
    <div id="webbanner-singlepage" class="webbanner-pagetype" style="min-height:250px;">
        <?php include('content/singlepage/index.php');?>
    </div>
    <div id="webbanner-channelpage" class="webbanner-pagetype" style="min-height:250px;">
        <?php include('content/channelpage/index.php');?>
    </div>
</div>