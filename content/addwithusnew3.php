<div class="main-center content">
    <div class="row">

        <div class="col-xs-12 col-sm-3 left">

            <ul class="tab-sidebar">
                <li><a href="#" data-value="1" data-target="webbaner" data-caption="Web Banner" class="active ads-type" id="webbanner">Web Banner </a></li>
                <li><a href="#" data-value="2" data-target="sponsor" data-caption="Sponsor" class="ads-type" id="sponsor">Sponsor</a></li>
            </ul>
            <input type="hidden" id="ads-type" value="1">
               
            <ul class="tab tab-inner two">
                <li><a class="active ads-res" data-value="1"  data-target="desktop" href="#desktop">Desktop </a></li>
                <li><a class="ads-res" data-value="2"  data-target="tablet" href="#tablet">Tablet</a></li>
                <li><a class="ads-res" data-value="3"  data-target="mobile" href="#mobile">Mobile</a></li>
            </ul>
            <input type="hidden" id="ads-res" value="1">

            <h2>Choose Channel :</h2>
            <div class="tab-content">
                <div id="webbanner" class="detail-tab active">
                    <ul class="list-group">
                        <li data-value="1" class="active"><a class="ads-channel" data-target="homepage" href="#homepage">Homepage ( Portal ) </a></li>
                        <li data-value="2"><a class="ads-channel" data-target="singlepage" href="#singlepage">Single Page</a></li>
                        <li data-value="3"><a class="ads-channel" data-target="channelpage" href="#channelpage">Channel Page</a></li>
                    </ul>
                </div>
                <div id="sponsor" class="detail-tab">
                    <ul class="list-group">
                        <li data-value="1" class="active"><a class="ads-channel"  data-target="advertorial" href="#advertorial">Advertorial </a></li>
                        <li data-value="2"><a class="ads-channel" data-target="inforial" href="#inforial">Inforial / Microsite</a></li>
                        <li data-value="3"><a class="ads-channel" data-target="videoads" href="#videoads">Video Ads</a></li>
                    </ul>
                </div>
                <input type="hidden" id="ads-channel" value="1">
             </div><!-- end tjp-tab-content -->

            
            <h5> For further info please contact :</h5>

            <h6>Aries Saputra</h6>
            <p> (62) 21 5300-476/78</p>
            <a href="#">aries@thejakartapost.com</a>
        </div> <!-- .left -->


        <div class="col-xs-12 col-sm-9 right"> 
            <div id="pagetype-wrap-webbanner" class="pagetype-wrap active">
                <?php $resols = array('desktop','tablet','mobile');?>
                <?php foreach($resols as $idx => $resol):?>
                    <?php if($resol == 'mobile'): ?>
                    <?php include('res-mobile.php');?>
                    <?php else :?>
                    <?php include('res-desktoptablet.php');?>
                    <?php endif;?>
                 <?php endforeach;?>
            </div>
            <div id="pagetype-wrap-sponsor" class="pagetype-wrap" style="padding-top:25px;">

                <?php foreach($resols as $idx => $resol):?>

                    <div id="resolution-sponsor-<?php echo $resol;?>" class="resolution-wrap <?php echo $idx==0?'active':'';?>">
                        <p><?php echo $resol;?></p>
                        <div id="sponsor-advertorial" class="sponsor-pagetype active">
                            <h1>Advetorial</h1>
                            <?php include('advertorial.php');?>
                        </div>
                        <div id="sponsor-inforial" class="sponsor-pagetype">
                            <h1>Inforial</h1>
                            <?php include('inforial.php');?>
                        </div>
                        <div id="sponsor-videoads" class="sponsor-pagetype">
                            <h1>Video Ads</h1>
                            <?php include('video.php');?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            
            
        </div> <!-- .right -->

    <style type="text/css">
    .dropview{
        width: 100%; height: 100%; text-align: center; background: #ccc;
    }
    </style>


    </div>
</div>
