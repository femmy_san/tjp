               <div class="main-center">
                <div class="col-xs-12 main-container no-padding">
                    <div class="col-xs-12 tjp-pargph-2 no-padding index-filter">
                        <div class="col-md-3 col-xs-12 container-left">
                            <h3 class="title-content">FILTER</h3>
                            <form action="#">
                                <div class="col-xs-12 filtering search-filter">
                                    <span class="subj-form">search about...</span>
                                    <input type="text" value="">
                                </div>
                                <div class="col-xs-12 filtering article-filter">
                                    <span class="subj-form">article form</span>
                                    <label>
                                        <input type="radio">news
                                    </label>
                                    <label>
                                        <input type="radio">image
                                    </label>
                                    <label>
                                        <input type="radio">video
                                    </label>
                                </div>
                                <div class="col-xs-12 filtering chanel-filter">
                                    <span class="subj-form">from this chanel</span>
                                    <label>
                                        <input type="radio">news
                                    </label>
                                    <label>
                                        <input type="radio">southeast Asia
                                    </label>
                                    <label>
                                        <input type="radio">community
                                    </label>
                                    <label>
                                        <input type="radio">Academia
                                    </label>
                                    <label>
                                        <input type="radio">Life
                                    </label>
                                    <label>
                                        <input type="radio">Travel
                                    </label>
                                    <label>
                                        <input type="radio">Jobs
                                    </label>
                                </div>
                                
                                <div class="col-xs-12 comment-filter">
                                    <a href="#">most viewed</a>
                                    <a href="#">most commented</a>
                                </div>
                                <div class="jak-post-issue col-xs-12 no-padding">
                                    <h3 class="title-issue">News Pulse</h3>
                                    <ul>
                                        <li><a href="#">ALL</a></li>
                                        <li><a href="#">#KAA2015</a></li>
                                        <li><a href="#">#AirAsia</a></li>
                                        <li><a href="#">#AirAsia</a></li>
                                        <li><a href="#">#FIFAScandal</a></li>
                                        <li><a href="#">#KAA2015</a></li>
                                        <li><a href="#">#AirAsia</a></li>
                                        <li><a href="#">#FIFAScandal</a></li>
                                        <li><a href="#">#KAA2015</a></li>
                                        <li><a href="#">#AirAsia</a></li>
                                        <li><a href="#">#FIFAScandal</a></li>
                                    </ul>
                                </div><!-- end jak-post-issue -->

                                <input type="submit" value="Search" class="filter-submit">
                            </form>
                        </div><!-- end container-left -->
                        <div class="col-md-6 col-xs-12 tjp-latest-entry">
                            <ul id="tjp-control-paging" class="block-grid-1 tjp-flag flag-latest">
                                <li class="ic-all ic-brunei">
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-1.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <div class="date-flag">
                                                <span class="date today">South East Asia <span>5 min ago</span></span>
                                                <span class="ic-flag flag-brn">brn</span>
                                            </div>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-1.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date today">Today’s <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-2.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date advert">Advertorial <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-3.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>
                                            <h5>The Future of the Seas in East Asia: Forging a Common Maritime Future</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-4.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date today">Today’s <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-5.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date news">News <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-1.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date today">News <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-5.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date news">News <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-2.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date news">News <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-4.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date today">Today’s <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-3.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>

                                            <h5>Mapping Policies and Programs of Maternal Mortality Rate in Indonesia: An Observation Through Policy Analysis and Design Framework</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-6.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>

                                            <h5>The Future of the Seas in East Asia: Forging a Common Maritime Future</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-2.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date advert">Advertorial <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-3.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>
                                            <h5>The Future of the Seas in East Asia: Forging a Common Maritime Future</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-5.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date news">News <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-2.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date news">News <span>5 min ago</span></span>
                                            <h5>Wealth report 'controversy' aimed at taking me down: Detective chief </h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-3.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>

                                            <h5>Mapping Policies and Programs of Maternal Mortality Rate in Indonesia: An Observation Through Policy Analysis and Design Framework</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-6.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date academia">Academia <span>5 min ago</span></span>
                                            <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">Deputy Executive Director of CSIS</small></span>

                                            <h5>The Future of the Seas in East Asia: Forging a Common Maritime Future</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="col-md-3 col-xs-4 image-latest">
                                            <img src="assets/img/latest-2.jpg" width="105" height="70" alt="Jakarta Post" />
                                        </div>
                                        <div class="col-md-9 col-xs-8 detail-latest">
                                            <span class="date advert">Advertorial <span>5 min ago</span></span>
                                            <h5>Blatter says he will resign as FIFA president</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor...</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <div class="navigation-page"></div>
                        </div>
                        <div class="col-md-3 col-xs-12 container-right">
                            <div class="line-1">
                                <a href="#">
                                    <img data-target="center" src="assets/img/after-slide-1.jpg" width="300" width="250" alt="Jakarta Post">
                                </a>
                                <a href="#">
                                    <img src="assets/img/after-slide-2.jpg" width="300" width="250" alt="Jakarta Post">
                                </a>
                            </div>
                            <div class="tjp-most-recent col-xs-12 no-padding">
                                <ul class="tjp-tab">
                                    <li><a href="#most-view">most-view</a></li>
                                    <li><a href="#most-commented">most commented</a></li>
                                </ul>
                                <div class="tjp-tab-content">
                                    <div id="most-view" class="tjp-detail-tab">
                                        <div class="scroll">
                                            <ul class="jak-post-latest-3">
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair1</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="most-commented" class="tjp-detail-tab">
                                        <div class="scroll">
                                            <ul class="jak-post-latest-3">
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair2</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Ahok scolds Djarot for messy folk fair</a></li>
                                                <li><a href="#">Wealth report 'controversy' aimed at taking me down: Detective chief</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- end tjp-tab-content -->
                            </div><!-- end tjp-most-recent -->
                            <div class="tjp-follow-sosmed col-xs-12 no-padding">
                                <h3 class="title-jakpost follow">follow us</h3>
                                <div class="col-sm-12 tjp-sosmed">
                                    <ul class="block-grid-1">
                                        <li class="soscmed-news">
                                            <span>News</span>
                                            <ul class="icon-soscmed">
                                                <li>
                                                    <a href="#" class="fa fa-fw facebook">&#xf09a;</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="fa fa-fw twitter">&#xf099;</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="fa fa-fw google">&#xf0d5;</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="soscmed-travel">
                                            <span>TRAVEL</span>
                                            <ul class="icon-soscmed">
                                                <li>
                                                    <a href="#" class="fa fa-fw instagram">&#xf16d;</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="fa fa-fw pint">&#xf231;</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="soscmed-jobs">
                                            <span>Jobs</span>
                                            <ul class="icon-soscmed">
                                                <li>
                                                    <a href="#" class="fa fa-fw linked">&#xf0e1;</a>
                                                </li>
                                                <li>
                                                    <a href="#" class="fa fa-fw twitter">&#xf099;</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- end tjp-follow-sosmed -->
                            <div class="jak-post-issue col-xs-12 no-padding">
                                <h3 class="title-issue">News Pulse</h3>
                                <ul>
                                    <li><a href="#"># KAA2015</a></li>
                                    <li><a href="#"># AirAsia</a></li>
                                    <li><a href="#"># AirAsia</a></li>
                                    <li><a href="#"># FIFAScandal</a></li>
                                    <li><a href="#"># KAA2015</a></li>
                                    <li><a href="#"># AirAsia</a></li>
                                    <li><a href="#"># FIFAScandal</a></li>
                                    <li><a href="#"># KAA2015</a></li>
                                    <li><a href="#"># AirAsia</a></li>
                                    <li><a href="#"># FIFAScandal</a></li>
                                </ul>
                            </div><!-- end jak-post-issue -->
                            <div class="other-chanel col-xs-12 no-padding">
                                <h2 class="title-jakpost chanel">From other chanel</h2>
                                <div class="col-md-12 no-padding">
                                    <a href="#" class="sea-chanel">
                                        <div class="image-title">
                                            <img src="assets/img/chanel-1.jpg" width="301" height="151" alt="Jakarta Post" />
                                            <span>SEA</span>
                                        </div>
                                        <span class="box-m">15th ASEAN Summit ends with successful outcomes</span>
                                    </a>
                                    <a href="#" class="academia-chanel">
                                        <div class="image-title">
                                            <img src="assets/img/chanel-2.jpg" width="301" height="151" alt="Jakarta Post" />
                                            <span>Academia</span>
                                        </div>
                                        <div class="detail-academia-chanel col-xs-12">
                                            <div class="col-lg-2 col-md-3 col-xs-4 image-latest">
                                                <img src="assets/img/latest-6.jpg" width="105" height="70" alt="Jakarta Post" />
                                            </div>
                                            <div class="col-lg-10 col-md-9 col-xs-8 detail-latest">
                                                <span class="profile-academia">Medelina K. Hendytio <small class="position-acedemia">A (Random) Rambler</small></span>

                                                <h5>The New Technology Brings Out a Bright Future</h5>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="community-chanel">
                                        <div class="image-title">
                                            <img src="assets/img/chanel-2.jpg" width="301" height="151" alt="Jakarta Post" />
                                            <span>Community</span>
                                        </div>
                                        <div class="detail-academia-chanel col-xs-12">
                                            <div class="col-lg-2 col-md-3 col-xs-4 image-latest">
                                                <img src="assets/img/latest-6.jpg" width="105" height="70" alt="Jakarta Post" />
                                            </div>
                                            <div class="col-lg-10 col-md-9 col-xs-8 detail-latest">
                                                <span class="name-profile">@S_Naulibasa</span>
                                                <span class="box-m">Five Special Hotel in Kemang Area</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div><!-- end chanel -->
                            <div class="col-xs-12 tjp-suscribe">
                                <img src="assets/img/suscribe.jpg" width="301" width="200" alt="Jak Post">
                                <div class="col-xs-12 get-suscribe">
                                    <a href="#" class="tjp-suscribe">Click to suscribe to our Newspaper</a>
                                    <a href="#" class="tjp-suscribe">Click to suscribe to our Digital Paper</a>
                                </div>

                            </div>
                        </div>
                    </div><!-- end tjp-pargph-2 -->
                </div>
                </div><!-- end main-center -->