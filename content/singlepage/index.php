<?php $sp_contents = array('news','sea','community','academia','life','travel','jobs','multimedia','youth');?>
<div>

    <!-- Nav tabs -->
    <ul id="webbanner-sp-tabs" class="nav nav-tabs" role="tablist">
        <?php foreach($sp_contents as $idx => $sp_content):?>
            <li role="presentation" class="<?php echo $idx==0?'active':'';?>">
                <a href="#tjp-singlepage-<?php echo $sp_content;?>" aria-controls="<?php echo $sp_content;?>" data-target="#tjp-singlepage-<?php echo $sp_content;?>" rel="<?php echo $sp_content;?>" role="tab" data-toggle="tab">
                    <?php $title = $sp_content=='sea'?'South East Asia':$sp_content; ?>
                    <?php echo ucfirst($title);?>
                </a>
            </li>
        <?php endforeach;?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php foreach($sp_contents as $idx => $sp_content):?>
            <div role="tabpanel" class="tab-pane  <?php echo $idx==0?'active':'';?>" id="#tjp-singlepage-<?php echo $sp_content;?>" rel="<?php echo $sp_content;?>" role="tab">
                <?php $title = $sp_content=='sea'?'South East Asia':$sp_content; ?>
                <h4 class="caption-no-content"><?php echo ucfirst($title);?></h4>
                <?php $inc_path = $sp_content.'-tab.php';?>
                <?php include($inc_path);?>
            </div>
        <?php endforeach;?>
    </div>

</div>